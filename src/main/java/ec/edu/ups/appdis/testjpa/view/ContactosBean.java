package ec.edu.ups.appdis.testjpa.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ec.edu.ups.appdis.testjpa.business.GestionContactos;
import ec.edu.ups.appdis.testjpa.model.Persona;
import ec.edu.ups.appdis.testjpa.model.Telefono;

@ManagedBean
@ViewScoped
public class ContactosBean {
	
	@Inject
	private GestionContactos on;

	private Persona newPersona;
	
	private List<Persona> listado;
	
	private String cedula;
	private String titulo = "Crear presonas";
	
	private boolean editable = false;
	

	@PostConstruct
	public void init() {
		newPersona = new Persona();
		newPersona.addTelefono(new Telefono());
		loadDataPersonas();
	}
	
	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		System.out.println("cedula parametro " + cedula);
		this.cedula = cedula;
		if(cedula!=null) {
			try {
				newPersona = on.getPersona(cedula);
				titulo = "Editar registro";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public Persona getNewPersona() {
		return newPersona;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setNewPersona(Persona newPersona) {
		this.newPersona = newPersona;
	}
	
	public List<Persona> getListado() {
		return listado;
	}
	public void setListado(List<Persona> listado) {
		this.listado = listado;
	}
	@Override
	public String toString() {
		return "ContactosBean [cedula=" + newPersona.getCedula() + ", nombre=" + newPersona.getNombre() + "]";
	}
	
	
	public String guardarDatos() {
		
		System.out.println(this.toString());
		
		
		
		try {
			on.guardarPersona(newPersona);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "listado";
	}
	
	public String guardarDatosAjax() {
		
		System.out.println(this.toString());
		
		
		
		try {
			on.guardarPersona(newPersona);
			loadDataPersonas();
			editable = false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String addTelefono() {
		newPersona.addTelefono(new Telefono());
		return null;
	}
	
	public String editar(String  cedula) {
		System.out.println(cedula);
		/*try {
			newPersona = on.getPersona(cedula);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return "crear?faces-redirect=true&cedula="+cedula;
	}
	
	public String editarAjax(String  cedula) {
		System.out.println(cedula);
		try {
			newPersona = on.getPersona(cedula);
			editable = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	private void loadDataPersonas() {
		try {
			listado = on.getPersonas();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

