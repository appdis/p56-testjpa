package ec.edu.ups.appdis.testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.testjpa.model.Curso;

@Stateless
public class CursoDAO {
	

	@PersistenceContext(name="testjpaPersistenceUnit")
	private EntityManager em;
	
	
	public void insert(Curso curso) {
		
		em.persist(curso);
	}
	
	public void update(Curso curso) {
		em.merge(curso);
	}
	
	public Curso read(int id) {
		return em.find(Curso.class, id);
	}
	
	public void delete(int id) {
		Curso p = read(id);
		em.remove(p);
	}
	
	public List<Curso> getCursos(String filtro){
		String jpql = "SELECT p FROM Curso p";
		
		Query q = em.createQuery(jpql, Curso.class);
		
		return q.getResultList();
	}
	
}
