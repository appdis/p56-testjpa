package ec.edu.ups.appdis.testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.testjpa.model.Inscripcion;

@Stateless
public class InscripcionDAO {
	

	@PersistenceContext(name="testjpaPersistenceUnit")
	private EntityManager em;
	
	
	public void insert(Inscripcion inscripcion) {
		
		em.persist(inscripcion);
	}
	
	public void update(Inscripcion inscripcion) {
		em.merge(inscripcion);
	}
	
	public Inscripcion read(int id) {
		return em.find(Inscripcion.class, id);
	}
	
	public void delete(int id) {
		Inscripcion p = read(id);
		em.remove(p);
	}
	
	public List<Inscripcion> getInscripcions(String filtro){
		String jpql = "SELECT p FROM Inscripcion p";
		
		Query q = em.createQuery(jpql, Inscripcion.class);
		
		return q.getResultList();
	}
	
}
