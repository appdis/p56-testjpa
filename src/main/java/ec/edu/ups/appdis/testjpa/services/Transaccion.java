package ec.edu.ups.appdis.testjpa.services;

public class Transaccion {

	
	private String cuentaOrigen;
	private String cuentaDestino;
	private double valor;
	
	
	public String getCuentaOrigen() {
		return cuentaOrigen;
	}
	public void setCuentaOrigen(String cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public String getCuentaDestino() {
		return cuentaDestino;
	}
	public void setCuentaDestino(String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {
		return "Transaccion [cuentaOrigen=" + cuentaOrigen + ", cuentaDestino=" + cuentaDestino + ", valor=" + valor
				+ "]";
	}
	
	
	
}
