package ec.edu.ups.appdis.testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.testjpa.model.Persona;

@Stateless
public class PersonaDAO {
	

	@PersistenceContext(name="testjpaPersistenceUnit")
	private EntityManager em;
	
	
	
	
	public void insert(Persona persona) throws Exception {
		
		em.persist(persona);
	}
	
	public void update(Persona persona) throws Exception {
		em.merge(persona);
	}
	
	public Persona read(String cedula) throws Exception {
		return em.find(Persona.class, cedula);
	}
	
	public void delete(String cedula) throws Exception {
		Persona p = read(cedula);
		em.remove(p);
	}
	
	public List<Persona> getPersonas(String filtro) throws Exception {
		String jpql = "SELECT p FROM Persona p "
				+ "WHERE nombre LIKE :filtro";
		
		Query q = em.createQuery(jpql, Persona.class);
		q.setParameter("filtro", filtro + "%");
		
		return q.getResultList();
	}
	
}
