package ec.edu.ups.appdis.testjpa.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.testjpa.dao.PersonaDAO;
import ec.edu.ups.appdis.testjpa.model.Persona;

@Stateless
public class GestionContactos {
	
	@Inject
	private PersonaDAO dao;

	public void guardarPersona(Persona p) throws Exception {
		Persona aux = dao.read(p.getCedula());
		
		if(aux!=null) {
			dao.update(p);
		}else {
			dao.insert(p);
		}
		
	}
	
	public List<Persona> getPersonas() throws Exception {
		return dao.getPersonas("%");
	}
	
	public Persona getPersona(String cedula) throws Exception {
		return dao.read(cedula);
	}
	
}
