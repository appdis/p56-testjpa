package ec.edu.ups.appdis.testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.testjpa.model.Alumno;

@Stateless
public class AlumnoDAO {
	

	@PersistenceContext(name="testjpaPersistenceUnit")
	private EntityManager em;
	
	
	public void insert(Alumno alumno) {
		
		em.persist(alumno);
	}
	
	public void update(Alumno alumno) {
		em.merge(alumno);
	}
	
	public Alumno read(int id) {
		return em.find(Alumno.class, id);
	}
	
	public void delete(int id) {
		Alumno p = read(id);
		em.remove(p);
	}
	
	public List<Alumno> getAlumnos(String filtro){
		String jpql = "SELECT p FROM Alumno p";
		
		Query q = em.createQuery(jpql, Alumno.class);
		
		return q.getResultList();
	}
	
	
	public Alumno getAlumno(int id){
		String jpql = "SELECT p FROM Alumno p"
				+ "	WHERE p.idAlumnno = :id";
		
		Query q = em.createQuery(jpql, Alumno.class);
		q.setParameter("id", id);
		
		/*List<Alumno> alumnos = q.getResultList();
		
		
		for(Alumno a: alumnos) {
			a.getDomicilio().getIdDomicilio();
		}*/
		
		
		Alumno alumno = (Alumno) q.getSingleResult();
		alumno.getDomicilio().getIdDomicilio();
		
		
		return alumno;
	}
	
}
