package ec.edu.ups.appdis.testjpa.view;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Contador{

	private int contador;
	
	private int aux;

	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador = contador;
	}
	
	
	
	public int getAux() {
		return aux;
	}
	public void setAux(int aux) {
		this.aux = aux;
	}
	
	public String doRecalcular(){
		this.contador = contador + 1;
		this.aux = this.contador * 100;
		System.out.println(this.contador + " - " + this.aux);
		
		return null;
	}
}
