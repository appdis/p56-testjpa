package ec.edu.ups.appdis.testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.testjpa.model.Domicilio;

@Stateless
public class DomicilioDAO {
	

	@PersistenceContext
	private EntityManager em;
	
	
	public void insert(Domicilio domicilio) {
		
		em.persist(domicilio);
	}
	
	public void update(Domicilio domicilio) {
		em.merge(domicilio);
	}
	
	public Domicilio read(int id) {
		return em.find(Domicilio.class, id);
	}
	
	public void delete(int id) {
		Domicilio p = read(id);
		em.remove(p);
	}
	
	public List<Domicilio> getDomicilios(String filtro){
		String jpql = "SELECT p FROM Domicilio p";
		
		Query q = em.createQuery(jpql, Domicilio.class);
		
		return q.getResultList();
	}
	
}
