package ec.edu.ups.appdis.testjpa.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.tags.Tag;

@OpenAPIDefinition(
        servers = {
                @Server(
                        description = "Servidor local",
                        url = "/testjpa")
        },
        tags = {
                @Tag(name = "OWA API", description = "Observatorio de Accesibilidad Web"),
                @Tag(name = "Tenon API", description = "Tenon IO"),
                @Tag(name = "AChecker API", description = "Web Accessibility Checker")
        }
)
@ApplicationPath("/rs")
public class RestApplication extends Application {

}
