package ec.edu.ups.appdis.testjpa.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.testjpa.dao.AlumnoDAO;
import ec.edu.ups.appdis.testjpa.dao.CursoDAO;
import ec.edu.ups.appdis.testjpa.dao.DomicilioDAO;
import ec.edu.ups.appdis.testjpa.dao.InscripcionDAO;
import ec.edu.ups.appdis.testjpa.dao.InstructorDAO;
import ec.edu.ups.appdis.testjpa.model.Alumno;
import ec.edu.ups.appdis.testjpa.model.Curso;
import ec.edu.ups.appdis.testjpa.model.Domicilio;
import ec.edu.ups.appdis.testjpa.model.Inscripcion;
import ec.edu.ups.appdis.testjpa.model.Instructor;

@Stateless
public class GestionCursos {

	
	@Inject
	private AlumnoDAO daoAlumno;
	
	@Inject 
	private DomicilioDAO daoDomicilio;
	
	@Inject 
	private CursoDAO daoCurso;
	
	@Inject 
	private InstructorDAO daoInstructor;
	
	@Inject 
	private InscripcionDAO daoInscripcion;
	
	public void registarAlumno() {
		
		Alumno alumno = new Alumno();
		alumno.setApellidoPaterno("Robles");
		alumno.setApellidoMaterno("Perez");
		alumno.setNombre("Lucas");
		
		
		Domicilio domicilio = new Domicilio();
		domicilio.setCalle("Totoracocha");
		domicilio.setCp("010103");
		domicilio.setCuidad("Cuenca");
		domicilio.setNoExterno("87");
		domicilio.setNoInterno("11");
		domicilio.setEstado(1);
		
		///daoDomicilio.insert(domicilio);
		
		alumno.setDomicilio(domicilio);
		
		
		daoAlumno.insert(alumno);	
		
	}
	
	public void registrarCurso(){
		
		Curso curso = new Curso();
		curso.setFechaInicio(new Date());
		curso.setFechaTermino(new Date());
		curso.setCuota(150);
		curso.setNombre("iOS");
		
		
		Instructor instructor = new Instructor();
		instructor.setIdInstructor(1);
		curso.setInstructor(instructor);
		
		daoCurso.insert(curso);
		
	}
	
	public void registrarInstructor() {
		Instructor ins = new Instructor();
		ins.setNombre("Luis");
		ins.setApellidoMaterno("Martinez");
		ins.setApellidoPaterno("Loja");
		
		daoInstructor.insert(ins);
	}
	
	
	public void registrarInstructorYCursos() {
		Instructor ins = new Instructor();
		ins.setNombre("Marcos");
		ins.setApellidoMaterno("Loyola");
		ins.setApellidoPaterno("Armijos");
		
		ins.setCursos(new ArrayList<Curso>());
		
		Curso c1 = new Curso();
		c1.setIdCurso(1);
		
		Curso c2 = new Curso();
		c2.setIdCurso(2);
		
		ins.addCurso(c1);
		ins.addCurso(c2);
		
		daoInstructor.insert(ins);
	}
	
	
	public void registrarInscripcion() {
		
		Inscripcion ins = new Inscripcion();
		
		
		Alumno alumno = new Alumno();
		alumno.setApellidoPaterno("Robles");
		alumno.setApellidoMaterno("Loja");
		alumno.setNombre("Marcelo");
		
		daoAlumno.insert(alumno);
		
		ins.setAlumno(alumno);
		
	
		Curso curso = new Curso();
		curso.setIdCurso(1);
		
		
		ins.setCurso(curso);
		
		ins.setHorario("Lunes 09:00 - 16:00");
		ins.setTipoCurso("Desarrollo");
		
		daoInscripcion.insert(ins);
		
	}
	
	public List<Alumno> getAlumnos(){
		return daoAlumno.getAlumnos("");
	}
	
	public List<Curso> getCursos(){
		return daoCurso.getCursos("");
	}
	
	public List<Instructor> getInstructores(){
		return daoInstructor.getInstructors("");
	}
	
	public List<Inscripcion> getInscripciones(){
		return daoInscripcion.getInscripcions("");
	}
	
	public Alumno getAlumno(int id) {
		return daoAlumno.getAlumno(id);
	}
	
	
}
