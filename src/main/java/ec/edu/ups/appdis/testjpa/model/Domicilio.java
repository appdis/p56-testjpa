package ec.edu.ups.appdis.testjpa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Domicilio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_domicilio")
	private int idDomicilio;
	
	private String calle;
	
	@Column(name="no_externo")
	private String noExterno;
	
	@Column(name="no_interno")
	private String noInterno;
	
	private String cp;
	
	private String cuidad;
	
	private int estado;
	
	/*@ManyToOne
	private Alumno alumno;*/
	
	/**
	 * 
	 * String jpql = "SELECT d FROM Domicilio d WHERE d.alumno.idAlumnno = :id"
	 * 
	 * 
	 * @return
	 */
	
	public int getIdDomicilio() {
		return idDomicilio;
	}
	public void setIdDomicilio(int idDomicilio) {
		this.idDomicilio = idDomicilio;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNoExterno() {
		return noExterno;
	}
	public void setNoExterno(String noExterno) {
		this.noExterno = noExterno;
	}
	public String getNoInterno() {
		return noInterno;
	}
	public void setNoInterno(String noInterno) {
		this.noInterno = noInterno;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getCuidad() {
		return cuidad;
	}
	public void setCuidad(String cuidad) {
		this.cuidad = cuidad;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	@Override
	public String toString() {
		return "Domicilio [idDomicilio=" + idDomicilio + ", calle=" + calle + ", noExterno=" + noExterno
				+ ", noInterno=" + noInterno + ", cp=" + cp + ", cuidad=" + cuidad + ", estado=" + estado + "]";
	}
	
	/*
	public Alumno getAlumno() {
		return alumno;
	}
	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	*/
	
	
}
