package ec.edu.ups.appdis.testjpa.view;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ec.edu.ups.appdis.testjpa.business.GestionContactos;
import ec.edu.ups.appdis.testjpa.business.GestionCursos;
import ec.edu.ups.appdis.testjpa.model.Alumno;
import ec.edu.ups.appdis.testjpa.model.Domicilio;
import ec.edu.ups.appdis.testjpa.model.Persona;


@WebServlet("/persona")
public class RegistroPersonas extends HttpServlet {

	@Inject
	private GestionContactos on;
	
	@Inject
	private GestionCursos gc;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.getWriter().println("<h1>Hello</h1>");
        
        /*Persona p = new Persona();
        p.setCedula("010370");
        p.setNombre("Juan");
        p.setEdad(10);
        
        on.guardarPersona(p);
        
        List<Persona> personas = on.getPersona();
        
        for(Persona p1 : personas) {
        	response.getWriter().println(p1.getNombre());
        }
        */
        
        
        //gc.registarAlumno();
        
        //gc.registrarInstructor();
        
        //gc.registrarCurso();
        //
        
        //gc.registrarInstructorYCursos();
        
        
        
        
        //gc.registrarInscripcion();
        
        
        List<Alumno> alumnos = gc.getAlumnos();
        for(Alumno p1 : alumnos) {
        	response.getWriter().println(p1);
        	System.out.println(p1);
        	Domicilio d = p1.getDomicilio();
        	//System.out.println(d);
        }
        
        Alumno alumno = gc.getAlumno(1);
        
        response.getWriter().println("<br><br>Datos personales: " + alumno);
        response.getWriter().println("<br><br>Datos domicilio: " + alumno.getDomicilio());
        
        
        
    }
	
	
}
