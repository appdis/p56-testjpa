package ec.edu.ups.appdis.testjpa.services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import ec.edu.ups.appdis.testjpa.business.GestionContactos;
import ec.edu.ups.appdis.testjpa.model.Persona;

@Path("/personas")
public class PersonasServiceRest {

	@Inject
	private GestionContactos gc;
	
	@GET
	@Path("/listado")
	@Produces("application/json")
	public String saludar(@QueryParam("x") String nombre) {
		return "Hola " + nombre;
	}
	
	
	@GET
	@Path("/listado/{name}/{usr}")
	@Produces("application/json")
	public String saludar2(@PathParam("name") String nombre, @PathParam("usr") String usuario) {
		return "Hola " + nombre;
	}
	
	@GET
	@Produces("application/json")
	public List<Persona> listaPersonas(){
		List<Persona> listado = new ArrayList<Persona>();
		
		try {
			listado = gc.getPersonas();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listado;
	}
	
	public String deposito(String cuentaOrigen, double valor) {
		// metodos que llaman al ON para realizar un deposito
		
		
		
		return null;
	}
	
	@POST
	@Produces("application/json")
	@Consumes("application/json")
	public Respuesta deposito(Transaccion trx) {
		// metodos que llaman al ON para realizar un deposito
		System.out.println(trx);
		
		Respuesta r = new Respuesta();
		r.setCodigo(1);
		r.setDescripcion("OK");
		return r;
	}
}
