package ec.edu.ups.appdis.testjpa.services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import ec.edu.ups.appdis.testjpa.business.GestionContactos;
import ec.edu.ups.appdis.testjpa.model.Persona;

@WebService
public class PersonasServiceSOAP {

	@Inject
	private GestionContactos gc;
	
	@WebMethod
	public String saludar(String nombre) {
		return "Hola " + nombre;
	}
	
	
	@WebMethod
	public List<Persona> listaPersonas(){
		List<Persona> listado = new ArrayList<Persona>();
		
		try {
			listado = gc.getPersonas();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listado;
	}
	
	@WebMethod
	public String deposito1(String cuentaOrigen, double valor) {
		// metodos que llaman al ON para realizar un deposito
		
		
		
		return null;
	}
	
	@WebMethod
	public String deposito(Transaccion trx) {
		// metodos que llaman al ON para realizar un deposito
		
		
		
		return null;
	}
	
	
	/*@WebMethod
	public String deposito2(String num, Movimiento mov) {
		// metodos que llaman al ON para realizar un deposito
		
		
		
		return null;
	}*/
}
