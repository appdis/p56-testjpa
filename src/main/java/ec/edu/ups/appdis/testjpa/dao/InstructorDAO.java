package ec.edu.ups.appdis.testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.appdis.testjpa.model.Instructor;

@Stateless
public class InstructorDAO {
	

	@PersistenceContext(name="testjpaPersistenceUnit")
	private EntityManager em;
	
	
	public void insert(Instructor instructor) {
		
		em.persist(instructor);
	}
	
	public void update(Instructor instructor) {
		em.merge(instructor);
	}
	
	public Instructor read(int id) {
		return em.find(Instructor.class, id);
	}
	
	public void delete(int id) {
		Instructor p = read(id);
		em.remove(p);
	}
	
	public List<Instructor> getInstructors(String filtro){
		String jpql = "SELECT p FROM Instructor p";
		
		Query q = em.createQuery(jpql, Instructor.class);
		
		return q.getResultList();
	}
	
}
